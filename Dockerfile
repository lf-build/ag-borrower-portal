FROM registry.lendfoundry.com/borrower-base
ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN npm install && rm -f .npmrc
EXPOSE 3000
COPY ./app /tenant
RUN apt-get update && \
    apt-get install ssh rsync --yes && \
    rsync --remove-source-files -a /tenant/ /tmp/app/ && \
    apt-get purge rsync --auto-remove --yes
ENTRYPOINT npm run start:production
FROM registry.lendfoundry.com/borrower-base
ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN npm install
RUN rm -f .npmrc
EXPOSE 3000
RUN npm run build:dll
ENTRYPOINT npm run start